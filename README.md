# File-Uploader

Copyright Kris Occhipinti 2024-03-19

(https://filmsbykris.com)

License GPLv3


original code https://alexey.shpakovsky.ru/en/busybox-httpd-file-upload.html

# Useage
```bash
sudo apt install busybox unzip wget

wget "https://gitlab.com/metalx1000/File-Uploader/-/archive/master/File-Uploader-master.zip"
unzip File-Uploader-master.zip
cd File-Uploader-master

port="8080"

echo "Starting server on port $port"
echo "http://localhost:$port"
busybox httpd -vvvf -p$port
```

